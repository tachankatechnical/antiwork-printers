

========================
ARE YOU BEING UNDERPAID?
========================

You have a protected LEGAL
RIGHT to discuss your pay
with your coworkers.

This should be done on a
regular basis to ensure that
everyone is being paid fairly.

It is ILLEGAL for your employer
to punish you for doing this.

If you learn that you are being
paid less than someone else who
is doing the same job, you
should demand a raise or
consider getting fired and
finding a better employer.

POVERTY WAGES only exist
because people are "willing"
to work for them.


Learn More:
=====================
reddit.com/r/antiwork
=====================









